DROP DATABASE IF EXISTS numbers;

CREATE DATABASE numbers;

\c numbers;

CREATE TABLE nums (
    id serial PRIMARY KEY,
    value int NOT NULL
);
