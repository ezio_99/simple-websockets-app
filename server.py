import asyncio
import random

import websockets
import asyncpg
import nest_asyncio

from config import db as db_config, LOW, UP, DELAY, START_ID, HOST, PORT

nest_asyncio.apply()


async def save_to_db() -> None:
    conn = await asyncpg.connect(**db_config)

    try:
        while True:
            value = random.randint(LOW, UP)
            print(f"Saving: {value}")
            await conn.execute("INSERT INTO nums (value) VALUES ($1)", value, )
            await asyncio.sleep(DELAY)
    finally:
        await conn.close()


async def get_from_db(conn, id_: int) -> (int, int):
    row = await conn.fetchrow('SELECT * FROM nums WHERE id = $1', id_, )
    return row[0], row[1]


async def nums(websocket, path):
    conn = await asyncpg.connect(user='postgres', password='postgres',
                                 database='numbers', host='127.0.0.1')
    try:
        id_ = START_ID
        while True:
            id, num = await get_from_db(conn, id_)
            id_ += 1
            print(f"Sending: {id} - {num}")
            await websocket.send(f"{id} - {num}")
            await asyncio.sleep(DELAY)
    finally:
        await conn.close()


def main():
    start_server = websockets.serve(nums, HOST, PORT)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start_server)
    loop.run_until_complete(save_to_db())


if __name__ == "__main__":
    main()
