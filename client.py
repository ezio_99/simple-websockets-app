import asyncio
import websockets

from config import HOST, PORT


async def get():
    uri = f"ws://{HOST}:{PORT}"
    async with websockets.connect(uri) as websocket:
        while True:
            msg = await websocket.recv()
            print(f"Received: {msg}")


def main():
    asyncio.get_event_loop().run_until_complete(get())


if __name__ == "__main__":
    main()
