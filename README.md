# Info

Extremely simple app with websockets.

Server: one thread save numbers to db, another reads from db and sends to websocket.

Client: get numbers from websocket.

# Init env

    ./init_db
    docker-compose up

# Install requirements

    pip3 install -r requirements.txt

# Run server

    python3 server.py

# Run client

    python3 client.py
